package com.egs.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionService {

    private static Connection connection;
    private static ConnectionService instance;
    private String dbUrl;
    private String dbUserName;
    private String dbPassword;
    private String dbDriverClassName;

    private ConnectionService() {
        loadProperties();
    }

    public static ConnectionService getInstance() {
        if (instance == null) {
            synchronized (ConnectionService.class) {
                if (instance == null) {
                    instance = new ConnectionService();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() {
            if (connection == null) {
                try {
                    Class.forName(dbDriverClassName);
                    connection = java.sql.DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

        return connection;
    }

    private void loadProperties() {
        Properties properties = new Properties();
        try (InputStream inStream =
                     ConnectionService.class.getClassLoader().getResourceAsStream("connection.properties")) {

            properties.load(inStream);

            dbUrl = properties.getProperty("db.url");
            dbUserName = properties.getProperty("db.username");
            dbPassword = properties.getProperty("db.password");
            dbDriverClassName = properties.getProperty("db.driverClassName");

        } catch (IOException ioe) {
            throw new IllegalStateException(ioe);
        }
    }
}
