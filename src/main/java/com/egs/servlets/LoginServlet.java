package com.egs.servlets;

import com.egs.service.ConnectionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    Connection connection;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/views/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("username");
        String password = req.getParameter("password");

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
                    "INSERT INTO user_db.user (username, password) " +
                        "VALUES (?,?)");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            statement.setString(1, userName);
            statement.setString(2, password);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionService.getInstance().getConnection();
    }
}
